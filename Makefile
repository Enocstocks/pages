.PHONY: export

export: index.html about.html agenda.html entries/tirc.html entries/relicensing.html \
	entries/using-bison.html entries/refusing-go.html assets/style.css \
	entries/usb-reverse-engineering.html entries/dumper-gcc.html \
	selfish-corner/the-self.html selfish-corner/screen/index.html \
	selfish-corner/ratpoison/index.html selfish-corner/gnu-global/index.html \
	code-analysis/input-keyboard-linux.html non-tech/german.html

%.html: %.org
	emacs -l config.el $^ -batch -f org-html-export-to-html
